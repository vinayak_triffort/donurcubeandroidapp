package com.example.donorcube;

import android.R.string;
import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends Activity {
	
	public static String sign_up = "signup";
	public static String login_name = "login";
	public static String email_id = "email";
	public static String email_password = "password";
	public static String remember_me = "remember";
	public static String sign_in = "signin";
	public static String forgotpassword = "forgotpassword";
	public static String mobileSercurity = "mobileSecurity";
	public static String about_us = "aboutus";
	
	
	Button signup, signin ,forgotPassword ;
	TextView login,mobileSecruity, aboutus; 
	EditText email;
	EditText password ;
	CheckBox remember;

	 
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_donorcube);
		intcomponents();
	
	}



	private void intcomponents() {
		// TODO Auto-generated method stub
		
		Typeface custome_font = Typeface.createFromAsset(getAssets(), "Font/Helvetica_Regular.ttf");
		
		signup = (Button)findViewById(R.id.signup);
		login = (TextView)findViewById(R.id.login);
		email = (EditText)findViewById(R.id.email);
		password = (EditText)findViewById(R.id.password);
		remember = (CheckBox)findViewById(R.id.remember);
		signin = (Button)findViewById(R.id.signin);
		forgotPassword = (Button)findViewById(R.id.forgotPassswrdButton);
		mobileSecruity = (TextView)findViewById(R.id.mobileSecurity);
		aboutus = (TextView)findViewById(R.id.aboutus);
		
		signup.setTypeface(custome_font);
		login.setTypeface(custome_font);
		email.setTypeface(custome_font);
		password.setTypeface(custome_font);
		remember.setTypeface(custome_font);
		signin.setTypeface(custome_font);
		forgotPassword.setTypeface(custome_font);
		mobileSecruity.setTypeface(custome_font);
	
	}
}
